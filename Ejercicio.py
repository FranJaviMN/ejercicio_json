import json
from Funciones import *

with open('playerdata.json') as fichero:
    datos=json.load(fichero)

print(mostrar_menu())

eleccion=int(input('Eliga una opcion del menu: '))

while eleccion != 6:

    if eleccion == 1:
        for jugadores in jugador_draft(datos):
            print('El jugador',jugadores['Nombre']+' '+jugadores['Apellido'],'fue escogido en el draft del año',jugadores['Year'])
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))

    if eleccion == 2:
        print('En la NBA hay',cant_jugadores(datos),'jugadores jugando.')
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))
    
    if eleccion == 3:
        nacionalidad=str(input('Dime una nacionalidad: '))
        for jugadores in nacionalidad_jugador(datos,nacionalidad):
            print('El jugador',jugadores['Nombre']+' '+jugadores['Apellido'],'es de la nacionalidad',nacionalidad)
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))

    if eleccion == 4:
        year=str(input('Dime el año de draft que quieras consultar: '))
        for jugadores in draft_año(datos,year):
            print('El jugador',jugadores['Nombre']+' '+jugadores['Apellido'],'fue elegido en el draft del año',year)
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))

    if eleccion == 5:
        for jugadores in draft_1(datos):
            print('El jugador',jugadores['Nombre']+' '+jugadores['Apellido'],'fue elegido el numero uno del draft del año',jugadores['Año Draft'])
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))
    
    if eleccion > 6:
        print('Error, esa opcion no es validad.')
        print(mostrar_menu())
        eleccion=int(input('Dime la opcion que quieres escoger: '))

if eleccion == 6:
    print('Saliendo del programa...')