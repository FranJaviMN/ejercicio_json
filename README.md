# Ejercicio_json

En este proyecto tenemos que realizar un ejercicio en Python que trabajara sobre un fichero .json .
El ejercicio que vamos a realizar se hara sobre el siguiente fichero: [fichero.json](https://gitlab.com/FranJaviMN/ejercicio_json/-/blob/master/playerdata.json)

## ENUNCIADO EJERCICIO EN PYTHON
* 1. Lista los años de draft y los jugadores elegidos ese año.
* 2. Cuenta la cantidad de jugadores que hay en la liga.
* 3. Se pide por teclado una nacionalidad y se muestra todos los jugadores de esa nacionalidad.
* 4. Se pide por teclado un año y se muestra todos los jugadores elegidos en el Draft de ese año.
* 5. Lista los jugadores escogidos el numero 1 del draft.


