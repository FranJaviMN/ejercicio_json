# Funcion que nos muestra los jugadores elegidos en los distintos años.

def jugador_draft(datos):
    diccionario_jugadores={}
    lista_jugadores=[]
    for jugadores in datos['league']['standard']:
        diccionario_jugadores['Year']=jugadores['draft']['seasonYear']
        diccionario_jugadores['Nombre']=jugadores['firstName']
        diccionario_jugadores['Apellido']=jugadores['lastName']
        lista_jugadores.append(diccionario_jugadores)
        diccionario_jugadores={}
    return lista_jugadores

# Funcion que nos muestra la cantidad de jugadores que hay en liga NBA.

def cant_jugadores(datos):
    lista_jugadores=[]
    for jugadores in datos['league']['standard']:
        lista_jugadores.append(jugadores['firstName'])
    return len(lista_jugadores)

# Funcion que pide una nacionalidad y nos devuelve los jugadores de esa nacionalidad.

def nacionalidad_jugador(datos,nacionalidad):
    lista_jugadores=[]
    diccionario_jugadores={}
    for jugadores in datos['league']['standard']:
        if jugadores['country'] == nacionalidad:
            diccionario_jugadores['Nombre']=jugadores['firstName']
            diccionario_jugadores['Apellido']=jugadores['lastName']
            lista_jugadores.append(diccionario_jugadores)
            diccionario_jugadores={}
    return lista_jugadores

# Funcion que nos pide un año y nos muestra todos los jugadores que ha sido elegidos en el draft de ese año.

def draft_año(datos,year):
    lista_jugadores=[]
    diccionario_jugadores={}
    for jugadores in datos['league']['standard']:
        if year == jugadores['draft']['seasonYear']:
            diccionario_jugadores['Nombre']=jugadores['firstName']
            diccionario_jugadores['Apellido']=jugadores['lastName']
            lista_jugadores.append(diccionario_jugadores)
            diccionario_jugadores={}
    if len(lista_jugadores) == 0:
            return print('No hay jugadores que hayan sido Drafteados ese año.')
    else:
        return lista_jugadores

# Funcion que nos muestra los jugadores elegidos el numero 1 del draft de cada año.

def draft_1(datos):
    lista_jugadores=[]
    diccionario_jugadores={}
    for jugadores in datos['league']['standard']:
        if jugadores['draft']['roundNum'] == '1' and jugadores['draft']['pickNum'] == '1':
            diccionario_jugadores['Nombre']=jugadores['firstName']
            diccionario_jugadores['Apellido']=jugadores['lastName']
            diccionario_jugadores['Año Draft']=jugadores['draft']['seasonYear']
            lista_jugadores.append(diccionario_jugadores)
            diccionario_jugadores={}
    return lista_jugadores

def mostrar_menu():
    menu='''
    --------------------MENU--------------------
    1. Lista los años de draft y los jugadores elegidos ese año.
    2. Cuenta la cantidad de jugadores que hay en la liga.
    3. Se pide por teclado una nacionalidad y se muestra todos los jugadores de esa nacionalidad.
    4. Se pide por teclado un año y se muestra todos los jugadores elegidos en el Draft de ese año.
    5. Lista los jugadores escogidos el numero 1 del draft.
    6. Salir del programa
    '''
    return menu